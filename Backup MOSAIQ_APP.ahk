﻿/*
Version History
---------------

	version 0.2 - C.P - 2018 May 01:
	updated logic to check for MOSAIQ_APP share path.
	added progress bar to zip files


	version 0.1 - C.P - 2018 Apr 26:
	Created Tool
*/
/*
	! ! ! --- READM ME & USAGE --- ! ! !
		This Project allows automatic backup of lose files and selected folders within the MOSAIQ_APP dir
		it will detect the path automaticlly, and when not it will prompt the user for a path
		(by checking the path has an accwin.exe present)
*/
/*
Variables and global settings
-----------------------------
*/
	toolVersion=0.2
	toolName=BackupMQAPP
	WinTitle=Backup MOSAIQ_APP
	#NoEnv			;	Recommended for performance and compatibility with future AutoHotkey releases.
	#SingleInstance Force
	#NoTrayIcon	
	MOSAIQ_APP_Dir := (comobjcreate("wscript.shell").exec(comspec " /c wmic share where name='MOSAIQ_APP' get Path | Findstr /R [:] ")).stdout.readall() ; wmic via DOS
	;;MOSAIQ_APP_Dir := ComObjGet("winmgmts:{impersonationLevel=impersonate}!\\.\ROOT\cimv2").ExecQuery("SELECT Path FROM Win32_Share.Name='MOSAIQ_APP'")._NewEnum() ; wmic via AHK - not working
	;ListVars		;	enable this to show AHK Debug view
	;#Warn			;	Enable warnings to assist with detecting common errors.
	SendMode Input	;	Recommended for new scripts due to its superior speed and reliability.
	SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
	
	FileInstall, 7za.exe, %A_ScriptDir%\7za.exe, 1
/* 
Prog 1 - Main Functions
--------------------
*/
MOSAIQ_APP_Dir := RegExReplace(MOSAIQ_APP_Dir, "(^\s*|\s*$)") ; trim variable
folderlogic:
try ;Attempt to find the MOSAIQ_APP Directory from the shares list, else prompt for location
	{
		If (MOSAIQ_APP_Dir)=""
			{
			msgbox 49, MOSAIQ_APP, MOSAIQ_APP Path not found `nPress OK to locate.
			IfMsgBox OK
				{
				FileSelectFolder, MOSAIQ_APP_Dir , , 2, Please Locate MOSAIQ_APP Folder
				If MOSAIQ_APP_Dir =""
					{
					goto, exit ; Folder not selected
					}
				}
				IfMsgBox Cancel
					{
					goto, exit ;  user cancelled.
					}
			}
		MOSAIQ_APP_Working = %MOSAIQ_APP_Dir%\Working
		If (InStr(!FileExist(MOSAIQ_APP_Working),"D"))
			{
			fileCreateDir, %MOSAIQ_APP_Dir%\Working
			;Working Path created, Continue with Try
			}
		MQReleaseFolder:
		FileSelectFolder, MOSAIQ_APP_Working , %MOSAIQ_APP_Dir%\Working , 3, Please Locate MOSAIQ Release Folder (e.g. ..\Working\2.64.xxx
		If MOSAIQ_APP_Working =""
			{
			MsgBox No Folder selected!
			Return
			}	
	}
Catch 
	{
	MsgBox 16, Error!, MOSAIQ_APP could not be determined, this application will now close!, 3
	ExitApp
	}
backup:
if(MOSAIQ_APP_Working)!="" ;Check if path is not empty, continue with backup
	{
	MOSAIQ_APP_Backup = %MOSAIQ_APP_Working%\BackupPreUpgrade ; create backup folder
	If (InStr(!FileExist(MOSAIQ_APP_Backup),"D"))
		{
		fileCreateDir, %MOSAIQ_APP_Backup%
		}
	else
		{
		Progress, b w200, MOSAIQ_APP.zip, Compressing Files..., Backup MQ Files
		Progress, 10
		RunWait, %A_ScriptDir%\7za.exe a -tzip -mx1 "%MOSAIQ_APP_Backup%\MOSAIQ_APP.zip" "%MOSAIQ_APP_Dir%\*.*", ,hide
		Progress, 20, Mergecom.zip
		RunWait, %A_ScriptDir%\7za.exe a -tzip -mx1 "%MOSAIQ_APP_Backup%\Mergecom.zip" "%MOSAIQ_APP_Dir%\Mergecom\*", ,hide
		Progress, 30, MLC.zip
		RunWait, %A_ScriptDir%\7za.exe a -tzip -mx1 "%MOSAIQ_APP_Backup%\MLC.zip" "%MOSAIQ_APP_Dir%\MLC\*", ,hide
		Progress, 40, VMI.zip
		RunWait, %A_ScriptDir%\7za.exe a -tzip -mx1 "%MOSAIQ_APP_Backup%\VMI.zip" "%MOSAIQ_APP_Dir%\VMI\*", ,hide
		Progress, 50, Reports.zip
		RunWait, %A_ScriptDir%\7za.exe a -tzip -mx1 "%MOSAIQ_APP_Backup%\Reports.zip" "%MOSAIQ_APP_Dir%\Reports\*", ,hide
		Progress, 60, Custom Reports.zip
		RunWait, %A_ScriptDir%\7za.exe a -tzip -mx1 "%MOSAIQ_APP_Backup%\Custom Reports.zip" "%MOSAIQ_APP_Dir%\Custom Reports\*", ,hide
		Progress, 70, RTP.zip
		RunWait, %A_ScriptDir%\7za.exe a -tzip -mx1 "%MOSAIQ_APP_Backup%\RTP.zip" "%MOSAIQ_APP_Dir%\RTP\*", ,hide
		Progress, 80, INIs.zip
		RunWait, %A_ScriptDir%\7za.exe a -tzip -mx1 "%MOSAIQ_APP_Backup%\INIs.zip" "%MOSAIQ_APP_Dir%\INIs\*", ,hide
		Progress, 90
		Progress, OFF
		}
	}
else
	{
	return
	}
exit:
OnExit
FileDelete, %A_ScriptDir%\7za.exe
/* 
GUI 1 - Main GUI
--------------------
*/